#ifndef __STACK_H__
#define __STACK_H__

#include "tdataroot.h"
#include <iostream>
using namespace std;

Class TStack: public TDataRoot
{
private :
	int top;
	virtual int GetNextIndex(int index) {return ++index;}
public
	TStack(int Size = DefMemSize): TDataRoot(Size)
	{
		top = -1;
	}
	void  Put   (const TData &Val);
	TData Get   ();
	int IsValid();
	void Print();
	int GetTop();
}
void TStack:: Put(const TData& Val)
{
	if(pMem = nullptr)
		SetRedCode(DataNoMem);
	else
	{
		if(IsFull())
		void *p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++top] = Val;
		DataCount++;
	}
		else
	{
		top = GetNextIndex(top);
		pMem[top] = Val;
		DataCount++;
	}
}
TData TStack:: Get()
{
	TData temp = -1;
	if(pMem = nullptr)
		SetRedCode(DataNoMem);
	else
		if(IsEmpty())
			SetRedCode(DataEmpty);

	else
	{
		temp = pMem[top--];
		DataCount--;
	}
	return temp;
}
int TStack:: IsValid()
{
	int res = 0;
	if(pMem == nullptr || MemSize < DataCount)
		res++;
	return res;
}
void TStack:: Print()
{
	for(int i = 0; i < DataCount; i++)
		cout <<pMem[i] <<" ";
	cout <<endl;
}
int TStack:: GetTop()
{
	if(!IsValid())
		SetRedCode(DataNoMem);
	else 
		if(IsEmpty())
			SetRedCode(DataEmpty);
		else
			return pMem[top];
}
#endif