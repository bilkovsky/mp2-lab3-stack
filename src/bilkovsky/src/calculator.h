#ifndef __CALCULATOR_H__
#define __CALCULATOR_H__

#include <string>
#include <iostream>
#include "tdatstack.h"
#include "tsimplestack.h"
using namespace std;

enum SymbolsTypes { OPEN_BRACKET, CLOSE_BRACKET, SUM_OPERATIONS, MULTIPLICATION_OPERATION};


int GetSymbolPriority(char symbol) { // ���������� ��������� ������� ������� ������
	switch (symbol) {
	case '(':
		return OPEN_BRACKET;
		break;
	case ')':
		return CLOSE_BRACKET;
		break;
	case '+':
		return SUM_OPERATIONS;
		break;
	case '-':
		return SUM_OPERATIONS;
		break;
	case '*':
		return MULTIPLICATION_OPERATION;
		break;
	case '/':
		return MULTIPLICATION_OPERATION;
		break;
	default:
		return -1;
	}
}
int CheckBrackets(string str) { // �������� ���������� ����������� ������
	TStack brackets; // ���� ������
	int counter = 0; // ������� ������
	int pair_count = 0; // ������� ����� � ������� ������
	int ** brackets_table = new int * [255]; // ������� ������
	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		if (GetSymbolPriority(*iter) == OPEN_BRACKET){
			brackets.Put(pair_count);
			brackets_table[pair_count] = new int[2];
			brackets_table[pair_count][0] = ++counter;
			brackets_table[pair_count][1] = -1;
			pair_count++;
		}
		if (GetSymbolPriority(*iter) == CLOSE_BRACKET){
			if (brackets.IsEmpty()){
				brackets_table[pair_count] = new int[2];
				brackets_table[pair_count][0] = -1;
				brackets_table[pair_count][1] = ++counter;
				pair_count++;
			}
			else {
				brackets_table[brackets.Get()][1] = ++counter;
			}
		}
	}
	cout << "Brackets" << endl << "open | close" << endl;
	int errors_count = 0;
	for (int i = 0; i < pair_count; i++){
		if (brackets_table[i][0] == -1)
			errors_count++;
		if (brackets_table[i][1] == -1)
			errors_count++;
		cout << "  " << ((brackets_table[i][0] == -1) ? "-" : std::to_string(brackets_table[i][0])) << "  |  " 
			<< ((brackets_table[i][1] == -1) ? "-" : std::to_string(brackets_table[i][1])) << "  " << endl;
	}
	if (errors_count > 0)
		cout << "Errors count: " << errors_count << endl;
	else {
		cout << "No Errors" << endl;
	}
	return errors_count;
}
string toPostfix(string str){  // ������� � ����������� �����
	TStack operations;
	string tmp = "";
	bool operand_writing = false;
	string::iterator iter = str.begin();
	while (iter<str.end()) { // �������� �������� �� ������
		if (*iter == ' ') {
			str.erase(iter);
		}
		else
			iter++;
	}
	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		int priority = GetSymbolPriority(*iter);
		if (priority == -1){
			tmp += *iter;
			if (operand_writing == false)
				operand_writing = true;
		}
		else {
			if (operand_writing){
				operand_writing = false;
				tmp += " ";
			}
			if (priority == 1) {
				while (GetSymbolPriority(operations.GetVertex()) != 0) 
					tmp += operations.Get();
			}
			else if (priority == 0 || priority > GetSymbolPriority(operations.GetVertex()) || operations.IsEmpty())
				operations.Put(*iter);
			else {
				while (GetSymbolPriority(operations.GetVertex()) >= priority)
					tmp += operations.Get();
				tmp += " ";
				operations.Put(*iter);
			}
		}
	}
	while (!operations.IsEmpty()) {
		if (GetSymbolPriority(operations.GetVertex()) > 1)
			tmp += operations.Get();
		else
			operations.Get();
	}
	return tmp;
}

int isValueNumber(string s_param)
{
	for (int i = 0; i < s_param.size(); i++)
	{
		if ((s_param[i] >= '0') && (s_param[i] <= '9') || s_param[i] == '.')
			continue;
		else
			return 0;
	}
	return 1;
}

double CalculateFromPostfix(string str){
	TSimpleStack<double> operands;
	string operand;
	for (string::iterator iter = str.begin(); iter<str.end(); ++iter) {
		int priority = GetSymbolPriority(*iter);
		if (priority == -1 && *iter != ' ') {
			operand += *iter;
		}
		else {
			if (operand.length() > 0) {
				if (isValueNumber(operand))
					operands.push(atof(operand.c_str()));
				else {
					cout << "Operand is not a number" << endl;
					return 0;
				}
				operand.clear();
			}
			if (priority >= 2) { 
				double val[2], result = 0;
				for (int i = 0; i < 2; i++) {
					val[i] = operands.pop();
				}
				switch (*iter) {
				case '+':
					result = val[0] + val[1];
					break;
				case '-':
					result = val[1] - val[0];
					break;
				case '*':
					result = val[0] * val[1];
					break;
				case '/':
					result = val[1] / val[0];
					break;
				}
				operands.push(result);
			}
		}
	}
	return operands.pop();
}

double Calculate(string str){
	if (CheckBrackets(str) == 0)
		return CalculateFromPostfix(toPostfix(str));
	else {
		cout << "Error expression" << endl;
	}
	return 0;
}
#endif