#include "TSimpleStack"
#include <gtest.h>

TEST(TSimpleStack, stack_is_empty)
{
	TSimpleStack<int> ts;
	EXPECT_TRUE(ts.IsEmpty());
}
TEST(TSimpleStack, stack_is_full)
{
	TSimpleStack<int> ts;
	for(int i = 0; i < MemSize; i++)
		ts.push(i);
	EXPECT_TRUE(ts.IsFull());
}
TEST(TSimpleStack, pop_right_element)
{
	TSimpleStack<int> ts;
	ts.push(1);
	EXPECT_EQ(ts.pop(),1);
}
TEST(TSimpleStack, cant_get_from_empty_stack)
{
	TSimpleStack<int> ts;
	ASSERT_ANY_THROW(ts.Pop());
}
TEST(TSimpleStack, cant_put_element_in_full_stack)
{
	TSimpleStack<int> ts;
	for (int i = 0; i < MemSize; i++)
		ts.Push(i);
	ASSERT_ANY_THROW(ts.Push(0));
}