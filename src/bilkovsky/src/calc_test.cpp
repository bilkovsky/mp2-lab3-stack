#include <calculator.h>
#include <gtest.h>
TEST(calc, check_brackets)
{
	std::string st = "(1+2*(3+4))";
	EXPECT_EQ(CheckBrackets(st),0);
}
TEST(calc, convert_to_postfix)
{
	std::string st = "(1+2*(3+4))";
	EXPECT_EQ(toPostfix(st), "1 2 3 4 +*+");
}
TEST(calc, is_value_number)
{
	EXPECT_TRUE(isValueNumber("4.54"));
}
TEST(calc, calculate_from_postfix)
{
	EXPECT_EQ(CalculateFromPostfix("1 2 3 4 +*+"),15);
}
TEST(calc, calculate)
{
	EXPECT_EQ(Calculate("(1+2*(3+4))"), 15);
}