#include "tdatstack.h"
#include <gtest.h>

TEST(TStack, can_create_stack_with_positive_length) 
{
	ASSERT_NO_THROW(TStack st(5));
}
TEST(TStack, throws_when_create_stack_with_negative_length)
{
	ASSERT_ANY_THROW(TStack st(-1));
}
TEST(TStack, stack_is_empty) 
{
	TStack st;
	st.Put(1);
	st.Get();
	EXPECT_TRUE(st.IsEmpty());
}
TEST(TStack, stack_is_full) 
{
	TStack st(1);
	st.Put(1);
	EXPECT_TRUE(st.IsFull());
}
TEST(TStack, get_right_element)
{
	TStack st(1);
	st.Put(1);
	EXPECT_EQ(st.Get(), 1);
}
TEST(TStack, cant_get_from_empty_stack)
{
	TStack st;
	st.Get();
	EXPECT_EQ(DataEmpty, st.GetRetCode());
}
TEST(TStack, ret_code_test)
{
	TStack st;
	st.Put(1);
	EXPECT_EQ(DataOK, st.GetRetCode());
}
TEST(TStack, can_get_elem)
{
	TStack st(1);
	st.Put(1);
	EXPECT_EQ(1, st.Get());
}
TEST(TStack, can_put_element_in_full_stack)
{
	TStack st(1);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(2, st.Get());
}
TEST(TStack, can_get_ret_code_is_empty)
{
	TStack st(1);
	st.Get();
	EXPECT_EQ(DataEmpty, st.GetRetCode());
}