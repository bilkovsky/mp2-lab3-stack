#ifndef __TSIMPLESTACK__
#define __TSIMPLESTACK__

#define DefMemSize 25
template<class ValType>
Class TSimpleStack
{
private:
	int top;
	ValType Mem[DefMemSize]
public:
	TSimpleStack {top = -1;}
	bool IsEmpty() const {return top == -1;}
	bool IsFull() const {return top == DefMemSize - 1;}
	void push(const ValType& val);
	ValType pop();
};
template<class ValType>
void TSimpleStack:: push(const ValType& val)
{
	If(IsFull())
		throw ("Stack is full")
	else
		Mem[++top] = val;
}
ValType TSimpleStack:: pop()
{
	if(IsEmpty())
		throw ("Stack is empty");
	else
		return Mem[--top];
}

#endif